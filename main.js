import * as titleHandler from './title-handler.js';
import * as dataService from './data.js';
import * as dnd from './dnd.js';



const data = dataService.generate();
dataService.setCallback(() => {
    titleHandler.init();
});
dataService.fillTemplate(data);

dnd.init('.content');
