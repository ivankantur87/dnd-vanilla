export function init() {
    const titles = document.getElementsByClassName("title");

    for (let div of titles) { 
        const handleClick = () => {
            const title = div.innerHTML;
            div.removeEventListener("click", handleClick);
            div.innerHTML = `
                <input type="text" value="${title}" placeholder="Название">
                <button>Готово</button>
            `;
            const button = div.getElementsByTagName("button")[0];
            const input = div.getElementsByTagName("input")[0];
            button.addEventListener("click", () => {
                // тут отправка данных на сервер

                const newTitle = input.value;
                div.innerHTML = newTitle || title;


                setTimeout(() => {
                    div.addEventListener("click", handleClick);
                }, 0);
            })
        }
        div.addEventListener("click", handleClick);
    }   
}
