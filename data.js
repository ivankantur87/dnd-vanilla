const container = document.querySelector('.content');
let callback;
export function generate() {
    const data = [];

    for (let i=0; i<20; i++){
        const product = {
            id: i,
            title: `Название ${i+1}`,
            options: ['Параметр 1', 'Параметр 2', 'Параметр 3'],
            price: Math.round(Math.random() * 100 + 100, -2),
        }
        data.push(product)
    }
    return data;
}

export function fillTemplate(data) {
    container.innerHTML = ''; 
    data.forEach(product => {
        const tile = document.createElement("div");
        tile.setAttribute("idd", product.id);
        tile.className =  'tile';
        tile.innerHTML = `
            <div class="title">${product.title}</div>
            ${product.options?.length
                ? `<div class="options">
                        ${product.options.map(option => (`<div>${option}</div>`)).join('')}                        
                    </div>`
                : ''
            }            
            <div class="price">${product.price}</div>
            <button>Купить</button>`;
        container.appendChild(tile);
    });
    callback();
}

export function setCallback(cb) {
    callback = cb;
}


