let container, element, placeholder, sideOld, belowId = null, bbox, startX, startY;
export function init(parentContainerClass) {
  container = document.querySelector(parentContainerClass);
  container.addEventListener('pointerdown', userPressed, { passive: true });
}

function userPressed(event) {
  if (['INPUT', 'BUTTON'].includes(event.target.tagName)) return;
  element = findParentWithClass(event.target);
  if (!element) return;

  element.ondragstart = function() { return false; }

  startX = event.clientX;
  startY = event.clientY;
  bbox = element.getBoundingClientRect();
  
  
  element.style.width = `${element.clientWidth}px`;
  element.style.height = `${element.clientHeight}px`;
  element.classList.add("tile-dragged");
  element.style.left = bbox.left + "px";
  element.style.top = bbox.top + "px";

  createPlaceholder();
  container.insertBefore(placeholder, element);

  container.addEventListener('pointermove', elementMoved, { passive: true });
  container.addEventListener('pointerup', elementReleased, { passive: true });
  container.addEventListener('pointercancel', elementReleased, { passive: true });
}

function elementMoved(event) {
  moveDraggedElement(event);

  const elemBelow = getElementBelow(event);
  if (!elemBelow) return;
    
  const side = checkSide(elemBelow, event);

  // check belowId for changes
  const nodes = [...container.childNodes].filter(el => !(el.classList.contains('placeholder')));
  const id = nodes.indexOf(elemBelow);
  const id2 = elemBelow.getAttribute('idd');
  console.log({ id, id2 });
  if (belowId !== id) {
    movePlaceholder(elemBelow, side);
    belowId = id;
  }

  // side changed
  if (!sideOld || sideOld !== side) {
    movePlaceholder(elemBelow, side);
    sideOld = side;
  }
}

function elementReleased(event) {
  element.classList.remove("tile-dragged");
  element.style.left = null;
  element.style.top = null;
  element.style.height = null;
  element.style.width = null;

  removePlaceholder();
  
  container.removeEventListener('pointermove', elementMoved);
  container.removeEventListener('pointerup', elementReleased);
  container.removeEventListener('pointercancel', elementReleased);

  // update container
  if (belowId !== null ) {
    const index = sideOld === 'right' ? +belowId + 1 : +belowId;
    container.insertBefore(element, container.children[index]);
    
    belowId = null;
    sideOld = null;
  }

}

function moveDraggedElement(event) {
  let deltaX = event.clientX - startX;
  let deltaY = event.clientY - startY;
  element.style.left = bbox.left + deltaX + "px";
  element.style.top = bbox.top + deltaY + "px";
}

function checkSide(elemBelow, event) {
  const bboxBelow = elemBelow.getBoundingClientRect();
  const deltaHalf = (bboxBelow.right - bboxBelow.left) / 2;
  const side = bboxBelow.left + deltaHalf > event.clientX ? 'left' : 'right';
  return side;
}

function createPlaceholder() {
  placeholder = document.createElement("div");
  placeholder.className =  "tile placeholder";
  placeholder.style.width = `${element.clientWidht}px`;
  placeholder.style.height = `${element.clientHeight}px`;
}

function removePlaceholder() {
  placeholder?.parentNode.removeChild(placeholder);
}

function movePlaceholder(elemBelow, side) {
  // вставляем элемент до если left или после
  if (side === 'left') {
    elemBelow.parentNode.insertBefore(placeholder, elemBelow);
  } else {
    elemBelow.parentNode.insertBefore(placeholder, elemBelow.nextSibling);
  }
}

function getElementBelow(event) {
  // позволяем словить элемент под перетягиваемым
  element.classList.add('display-none');
  let tempBelow = document.elementFromPoint(event.clientX, event.clientY);
  element.classList.remove('display-none');
  
  // если никого нет, то выход
  if (!tempBelow) return null;

  let elemBelow = tempBelow.closest('.tile'); //.findParentWithClass('tile');

  // если элемент placeholder, то отрабатываем как пустоту
  if (elemBelow === placeholder) return null;

  return elemBelow;
}

function findParentWithClass(element, className) {
  if (!element) return null;
  if (element.classList.contains(className)) {
    return element;
  }
  const parent = element.parentElement;
  if (parent === document) return null;
  return findParentWithClass(parent, className);
}
